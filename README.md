# envoy-proxy

WIP configs for a proxy for Envoy

## quick and dirty setup for testing, etc

### Prerequisites 

We're targeting Debian, so you'll need that installed. If you're installing fresh, leave the root password blank during install so it sets up sudo for you. If you don't, it just takes a little extra work to get sudo working.

You'll want to be able to ssh in to this system, probably passwordless (ie with a key). There's an `ssh-copy-id` helper script out there, but you can copy the key manually as well. You'll need to be able to `sudo` but it's ok if that needs a password.

To be clear, we just need to get ssh to the VM working, the following steps are all run on your "host" computer, not the target Debian system.

We'll need some self-signed certificates for testing, I used `mkcert` https://github.com/FiloSottile/mkcert

```
mkdir certs
cd certs
mkcert -install
```

The last step above creates a self signed Certificate Authority and installs it on your local system, you'll likely get password prompts

Now create a cert for the site:

```
mkcert example-wikipedia-proxy
```

generate the dhparam file

```
openssl dhparam -out  dhparam.pem 2048
cd ..
```

Finally we need to create an inventory file for ansible to find it's target host(s). Name it whatever you like, I'll use `inventory.ini` for this example. See below for more inventory examples for other services.

```
[envoy_proxies]
192.168.64.2
```

Replace that IP with the IP or host name of your proxy

We've generated a cert for a host called `example-wikipedia-proxy`. You may want to add a hosts file entry for that. It may be the path of least resistance to tell your client code to be ok with self signed for now, or you may need to add the CA to your test devices (see the mkcert docs above).

You can also generate a cert that's valid for the IP address, e.g.:

```
mkcert example-wikipedia-proxy 192.168.64.2
```

(note that the generated files have different names in this case, make sure to rename them to `example-wikipedia-proxy.pem` and `example-wikipedia-proxy-key.pem` or the playbook will fail)

### Setting up and running ansible

Create and activate a virtalenv:

```
python3 -m venv .
. bin/activate
pip install --upgrade pip
```

now install ansible-playbook:

```
pip install ansible
```

And we're pretty much ready to go. I've included an `apt.yaml` playbook, this one basically does `apt update && apt updgrade`, it's optional if your system is up to date:

```
ansible-playbook -i inventory.ini -K apt.yaml
```

It will ask for a "BECOME password" (because of the -K), that's your sudo password.


### Installing Specific services

All of these services can be installed on a single host or VM, or multiple.

Here's an example inventory with multiple hosts and services:

```
[envoy_proxies]
192.168.64.2
; 192.168.64.3
192.168.64.7

[obfs4proxy_servers]
192.168.64.2
192.168.64.7

[shadowsocks_servers]
192.168.64.2
192.168.64.7
```


#### Nginx proxy for envoy

Add your target(s) to the `envoy_proxies` section of your inventory file, e.g.:

```
[envoy_proxies]
192.168.64.2
```

Run the playbook:

```
ansible-playbook -i inventory.ini -K envoy_proxy.yaml
```

This is how I tested (this is based on an example in comments in the nginx config file):
```
curl --insecure -H 'Url-Orig: https://en.wikipedia.org/wiki/Main_Page' -H 'Host-Orig: en.wikipedia.org' https://192.168.64.2/wikipedia/
```

(the `--insecure` because it gets grumpy about the self signed certs otherwise)

you should see the HTML from Wikipedia. If you request a URL that's outside of Wikipedia, it should return an error.

#### obfs4proxy server

Add your target(s) to the `obfs4proxy_server` group in your inventory, e.g.:

```
[obfs4proxy_servers]
192.168.64.2
```

Run the playbook:

```
ansible-playbook -i inventory.ini -K obfs4proxy_server.yaml
```

Testing: ??? TODO

the server listens on port `55242`


#### shadowsocks-libev server

Add your target(s) to the `shadowsocks_servers` section of your inventory file, e.g.:

```
[shadowsocks_servers]
192.168.64.2
```

Run the playbook:

```
ansible-playbook -i inventory.ini -K shadowsocks_server.yaml
```

Testing: ??? TODO

The server listens on port `8388` with a default password of `aew4YahF2ox6yohj` and the `chacha20-ietf-poly1305` method is used by default. You can change both in the `shadowsocks-config.json` in this folder.

